import React, { Component } from 'react'
import { BrowserRouter, NavLink, Routes,Route } from 'react-router-dom';

//page
import Home from './views/Home'
import MyTask from './views/MyTask'
import Quotation from './views/Quotation'

class App extends Component {
  
 
  render() {
    let activeClassName = "nav-active"

    return (
      <div className="App">
        <div className="stypage">
          <BrowserRouter>
            <div className='menu4'>
              <div className='div-menu'>
                <NavLink to="/" className={({isActive}) =>  isActive ? activeClassName : undefined}>Home</NavLink>
                <NavLink to="/MyTask" className={({isActive}) =>  isActive ? activeClassName : undefined}>MyTask</NavLink>
                <NavLink to="/Quotation" className={({isActive}) =>  isActive ? activeClassName : undefined}>Quotation</NavLink>
              </div>
            </div>
            <Routes>
              <Route path='/' element={<Home/>}/>
              <Route path='/MyTask' element={<MyTask/>}/>
              <Route path='/Quotation' element={<Quotation/>}/>
            </Routes>
          </BrowserRouter>
        </div>
      </div>
    );
  }
}
export default App;
