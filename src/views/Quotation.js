import React, { Component } from "react";
import queryString from 'query-string'


export default class Header extends Component {
    state = {
        isLoading: true,
        gridSetting: {
            sort: [{ field: "DuedateStatus", dir: "desc" }],
            skip: 0,
            take: 10,
        },
        MyTask: [],
        
        };

        
        
        componentDidMount = async () => {
        let url = window.location.search
            ? window.location.search
            : window.location.pathname;
        let params = queryString.parse(url.toLowerCase());
        console.log(params);

        };
  render() {
    return (
      <div className="container pl-0 pr-0">
        <div className="text-center mt-5 mb-5 header-text">ขออนุมัติปล่อยเช่าพื้นที่ (Quotation / Contract)</div>
        <div className="boxlist">
          <div className="body-content"></div>
        </div>
        <div className="boxlist">
          <div className="headtext">รายละเอียดผู้สรา้ง (Requester Detail)</div>
          <div className="body-content">
            <div className="row">
              <div className="col-md-6">
              <table className="table table-bordered">
                <tbody>
                  <tr>
                    <td width={200}><p>Quotation No. (รหัสเอกสาร):</p></td>
                    <td><input type="text" className="form-control" aria-describedby="basic-addon3"/></td>
                  </tr>
                  <tr>
                    <td><p>Requester (ชื่อผู้สร้าง):</p></td>
                    <td><input type="text" className="form-control" aria-describedby="basic-addon3"/></td>
                  </tr>
                  <tr>
                    <td><p>Bu./Branch (หน่วยธุระกิจ/สาขา):</p></td>
                    <td><input type="text" className="form-control" aria-describedby="basic-addon3"/></td>
                  </tr>
                </tbody>
              </table>
              </div>
              <div className="col-md-6">
                <table className="table table-bordered">
                  <tbody>
                    <tr>
                      <td width={160}><p>Create Date (วันที่สร้าง):</p></td>
                      <td><input type="text" className="form-control" aria-describedby="basic-addon3"/></td>
                    </tr>
                    <tr>
                      <td><p>Com code (บริษัท):</p></td>
                      <td><input type="text" className="form-control" aria-describedby="basic-addon3"/></td>
                    </tr>
                    <tr>
                      <td><p>Sale Name (ชื่อผู้ชาย):</p></td>
                      <td><input type="text" className="form-control" aria-describedby="basic-addon3"/></td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
        <div className="boxlist">
          <div className="headtext">รายละเอียดผู้เช่า (Custumer Detail)</div>
          <div className="body-content">
            <div className="row">
              <div className="col-md-6">
                <table className="table table-bordered">
                  <tbody>
                    <tr>
                      <td width={200}>
                        <button type="button" className="btn btn-primary mr-1">search</button>
                        <button type="button" className="btn btn-danger">Reset</button>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <div className="col-md-6">
                <table className="table table-bordered">
                  <tbody>
                    <tr>
                      <td width={250}><p>Business partner Code  (รหัสลูกค้า (BP)):</p></td>
                      <td><input type="text" className="form-control" aria-describedby="basic-addon3"/></td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
            <div className="row">
              <div className="col-md-6">
                <table className="table table-bordered">
                  <tbody>
                    <tr>
                      <td width={250}><p>Custumer Name  (ชื่อผู้เช่า):</p></td>
                      <td>
                      <div className="input-group ">
                        <select className="custom-select" id="inputGroupSelect01">
                          <option selected>Please select</option>
                          <option value="1">One</option>
                          <option value="2">Two</option>
                          <option value="3">Three</option>
                        </select>
                      </div>
                      </td>
                    </tr>
                    <tr>
                      <td width={250}><p>Company Name (ชื่อสถานประกอกการ):</p></td>
                      <td>
                        <div className="input-group ">
                          <select className="custom-select" id="inputGroupSelect01">
                            <option selected>Please select</option>
                            <option value="1">One</option>
                            <option value="2">Two</option>
                            <option value="3">Three</option>
                          </select>
                        </div>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <div className="col-md-6">
                <table className="table table-bordered">
                  <tbody>
                    <tr>
                      <td><input type="text" className="form-control" aria-describedby="basic-addon3"/></td>
                    </tr>
                    <tr>
                      <td><input type="text" className="form-control" aria-describedby="basic-addon3"/></td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
            <div className="row">
              <div className="col-md-6">
                <table className="table table-bordered">
                  <tbody>
                    <tr>
                      <td width={250}><p>Shop Name (ชื่อร้านค้า):</p></td>
                      <td><input type="text" className="form-control" aria-describedby="basic-addon3"/></td>
                    </tr>
                    <tr>
                      <td><p>Company certificater No (หนังสือรับรองเลขที่):</p></td>
                      <td><input type="text" className="form-control" aria-describedby="basic-addon3"/></td>
                    </tr>
                    <tr>
                      <td><p>Certificater Date (วันที่ออกหนังสือรับรอง):</p></td>
                      <td>
                        
                      </td>
                    </tr>
                    <tr>
                      <td><p>Telephone (เบอร์ติดต่อ):</p></td>
                      <td><input type="text" className="form-control" aria-describedby="basic-addon3"/></td>
                    </tr>
                    <tr>
                      <td><p>Email (อีเมล):</p></td>
                      <td><input type="text" className="form-control" aria-describedby="basic-addon3"/></td>
                    </tr>
                    <tr>
                      <td><p>Business Type (ประเภทธุรกิจ):</p></td>
                      <td>
                        <div className="input-group ">
                          <select className="custom-select" id="inputGroupSelect01">
                            <option selected>Please select</option>
                            <option value="1">One</option>
                            <option value="2">Two</option>
                            <option value="3">Three</option>
                          </select>
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td><p>Sub Business Type (ธุระกิจ):</p></td>
                      <td><input type="text" className="form-control" aria-describedby="basic-addon3"/></td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <div className="col-md-6">
                <table className="table table-bordered">
                  <tbody>
                    <tr>
                      <td width={160}><p>Presonal ID (เลขที่บัตรประชาชน):</p></td>
                      <td><input type="text" className="form-control" aria-describedby="basic-addon3"/></td>
                    </tr>
                    <tr>
                      <td><p>Taxpayer Number (เลขที่ผู้เสียภาษี):</p></td>
                      <td><input type="text" className="form-control" aria-describedby="basic-addon3"/></td>
                    </tr>
                    <tr>
                      <td><p>Registerd capital (ทุนจดทะเบียน):</p></td>
                      <td><input type="text" className="form-control" aria-describedby="basic-addon3"/></td>
                    </tr>
                    <tr>
                      <td><p>Registerd number (ทะเบียนเลขที่):</p></td>
                      <td><input type="text" className="form-control" aria-describedby="basic-addon3"/></td>
                    </tr>
                    <tr>
                      <td><p>Special Reason (For BI):</p></td>
                      <td>
                        <div className="input-group ">
                          <select className="custom-select" id="inputGroupSelect01">
                            <option selected>Please select</option>
                            <option value="1">One</option>
                            <option value="2">Two</option>
                            <option value="3">Three</option>
                          </select>
                        </div>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
